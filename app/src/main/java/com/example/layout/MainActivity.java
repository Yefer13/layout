package com.example.layout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnLogin, btntLoginIn;
    EditText editTextUser, editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        btnLogin = findViewById(R.id.btnIniciarSesion);
        btntLoginIn = findViewById(R.id.buttonLoginIn);
        editTextUser =  findViewById(R.id.editTxtUsuario);
        editTextPassword = findViewById(R.id.editTextPassword);

        //Asignacion del evento click
        btnLogin.setOnClickListener(this);
        btntLoginIn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        //CheckBox checkBox = (CheckBox)v;
        //boolean seleccionado = checkBox.isChecked();
        Log.i("App","Click en login");

        String user = editTextUser.getText().toString();
        String pass = editTextPassword.getText().toString();

        if(!user.isEmpty() && !pass.isEmpty()){
        Intent loginIn = new Intent(this, HomeActivity.class);
        loginIn.putExtra("valueUser", user);
        startActivity(loginIn);
        }
        else {
            if (user.isEmpty()){
                editTextUser.setError("Complete el Usuario");
            }
            if( pass.isEmpty()){
                editTextPassword.setError("Complete el Password");
            }
        }
    }
}
