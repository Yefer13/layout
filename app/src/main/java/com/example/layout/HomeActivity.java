package com.example.layout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    TextView txtMostraUsuario;
    ListView listDate;
    List<String> androidVersionList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Bundle extras = getIntent().getExtras();
        String dateUser = extras.getString("valueUser");
        //Para que el usuario aparesca en el toolbar
        setTitle("Welcome : " + dateUser);

        txtMostraUsuario = findViewById(R.id.textViewMostraUsuario);
        txtMostraUsuario.setText(dateUser);

        //1 Conectar nuetsro listView (ListDate) al componente visual a través del id.
        listDate = findViewById(R.id.listView);

        //2. Cargar la lista de elementos
        androidVersionList = new ArrayList<>();
        androidVersionList.add("Pie");
        androidVersionList.add("Oreo");
        androidVersionList.add("Nougat");
        androidVersionList.add("Marshmallow");
        androidVersionList.add("Lollipop");
        androidVersionList.add("Kitkat");
        androidVersionList.add("....");

        //3. Adaptador

        ArrayAdapter adapterVersionAndroid = new ArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,
                androidVersionList
        );

        //4. Vinculacion de listView -adapter

        listDate.setAdapter(adapterVersionAndroid);

        //5. Gestion de envento click en la lista

        listDate.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String androidVersion = androidVersionList.get(position);
        Log.i("APP","Version click : " + androidVersion);
    }
}
